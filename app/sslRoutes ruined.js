// app/sslRoutes.js

var express 	  = require('express');
var moment      = require('moment');
var httpsRouter = express.Router();
var eagleData   = require('./models/eagleSchema');
var async       = require('async');
var jwt         = require('jsonwebtoken');


var jwtSecret = 'ThisIsASecret100';

	
     

	//do stuff every req or res
	httpsRouter.use(function(req, res, next){
		console.log(moment().format("ddd MMM Do YY, HH:mm:ss"));
		next();
	});	

	httpsRouter.get('/', function(req, res){
	
	res.sendfile('./public/app/index.html');
	//res.render('./pages/index.html');

	});

	httpsRouter.get('/api/history/7day', function(req, res) {

      var history = {

    kwhsNow: function (callback) {
        kwhsNowQuery = eagleData.eagleKwhs.find(),
          kwhsNowQuery.sort('-_id');
          kwhsNowQuery.limit(1);
          kwhsNowQuery.exec(function(err, data) {
            // if there is an error retrieving, send the error. nothing after res.send(err) will execute
            if (err)
                console.log(err);
                return callback(err)
            //console.log('Now: ',data[0].fromgrid);    
            callback(null, data);
        });
    }, 

    kwhsToday: function (callback) {
        var sinceToday = moment().hours(0).minutes(0).seconds(0).format('x');

        var sinceTodayQuery = eagleData.eagleKwhs.find();
          sinceTodayQuery.where('_id').lte(sinceToday); 
          sinceTodayQuery.limit(1);
          sinceTodayQuery.exec(function(err, data) {
            // if there is an error retrieving, send the error. nothing after res.send(err) will execute
            if (err)
                console.log(err);
                return callback(err);

            //console.log('Since today: ',data[0].fromgrid);  
            callback (null, data);

        });
    }, 

    kwhsDayAgo: function (callback) {
        var dayAgo = moment().subtract(1, 'days').hours(0).minutes(0).seconds(0).format('x');

        var dayAgoQuery = eagleData.eagleKwhs.find();
          dayAgoQuery.where('_id').lte(dayAgo); 
          dayAgoQuery.limit(1);
          dayAgoQuery.exec(function(err, data) {
            // if there is an error retrieving, send the error. nothing after res.send(err) will execute
            if (err)
                console.log(err);
                return callback(err);

            //console.log('Day Ago: ',data[0].fromgrid);    
            callback (null, data);
            
        }); 
    }, 

    kwhsTwoDaysAgo: function (callback) {
        var twoDaysAgo = moment().subtract(2, 'days').hours(0).minutes(0).seconds(0).format('x');

        var twoDaysAgoQuery = eagleData.eagleKwhs.find();
        twoDaysAgoQuery.where('_id').lte(twoDaysAgo); 
        twoDaysAgoQuery.limit(1);
        twoDaysAgoQuery.exec(function(err, data) {
        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
            if (err)
                console.log(err);
                return callback(err);

            //console.log('Two Days Ago: ',data[0].fromgrid);    
            callback (null, data);

        });

    }, kwhsThreeDaysAgo: function (callback) {
            var threeDaysAgo = moment().subtract(3, 'days').hours(0).minutes(0).seconds(0).format('x');

          var threeDaysAgoQuery = eagleData.eagleKwhs.find();
          threeDaysAgoQuery.where('_id').lte(threeDaysAgo); 
          threeDaysAgoQuery.limit(1);
          threeDaysAgoQuery.exec(function(err, data) {
            // if there is an error retrieving, send the error. nothing after res.send(err) will execute
            if (err)
                console.log(err);
                return callback(err);

            //console.log('Three Days Ago: ',data[0].fromgrid);    
            callback (null, data);
        });

    }, kwhsFourDaysAgo: function (callback) {
            var fourDaysAgo = moment().subtract(4, 'days').hours(0).minutes(0).seconds(0).format('x');    

          var fourDaysAgoQuery = eagleData.eagleKwhs.find();
          fourDaysAgoQuery.where('_id').lte(fourDaysAgo); 
          fourDaysAgoQuery.limit(1);
          fourDaysAgoQuery.exec(function(err, data) {
             //if there is an error retrieving, send the error. nothing after res.send(err) will execute
            if (err)
                console.log(err);
                return callback(err);

            //console.log('Four Days Ago: ',data[0].fromgrid);                   
            callback (null, data);
        });
    }, kwhsFiveDaysAgo: function (callback) {
        var fiveDaysAgo = moment().subtract(5, 'days').hours(0).minutes(0).seconds(0).format('x');    

      var fiveDaysAgoQuery = eagleData.eagleKwhs.find();
          fiveDaysAgoQuery.where('_id').lte(fiveDaysAgo); 
          fiveDaysAgoQuery.limit(1);
          fiveDaysAgoQuery.exec(function(err, data) {
            // if there is an error retrieving, send the error. nothing after res.send(err) will execute
            if (err)
                console.log(err);
                return callback(err);

            //console.log('Five Days Ago: ',data[0].fromgrid);   
            callback (null, data);
        });
    }, 

    kwhsSixDaysAgo: function (callback) {

        var sixDaysAgo = moment().subtract(6, 'days').hours(0).minutes(0).seconds(0).format('x');    

      var sixDaysAgoQuery = eagleData.eagleKwhs.find();
          sixDaysAgoQuery.where('_id').lte(sixDaysAgo); 
          sixDaysAgoQuery.limit(1);
          sixDaysAgoQuery.exec(function(err, data) {
            // if there is an error retrieving, send the error. nothing after res.send(err) will execute
            if (err)
                console.log(err);
                return callback(err);

            //console.log('Six Days Ago: ',data[0].fromgrid);    
            callback (null, data);
        });
    }, 

    kwhsSevenDaysAgo: function (callback){
        var sevenDaysAgo = moment().subtract(7, 'days').hours(0).minutes(0).seconds(0).format('x');    

      var sevenDaysAgoQuery = eagleData.eagleKwhs.find();
          sevenDaysAgoQuery.where('_id').lte(sevenDaysAgo); 
          sevenDaysAgoQuery.limit(1);
          sevenDaysAgoQuery.exec(function(err, data) {
            // if there is an error retrieving, send the error. nothing after res.send(err) will execute
            if (err)
                console.log(err);
                return callback(err);

            //console.log('Seven Days Ago: ',data[0].fromgrid);    
            callback (null, data);
            
        });
    } 
};

async.parallel (history, function (err, results) {

    if (err)
        console.log('Async: ', err);
        throw err;
    var kwhsMeter = results.kwhsNow[0].fromgrid;  
    //results holds the data in object form
    var inOrder = [results.kwhsToday[0], results.kwhsDayAgo[0], results.kwhsTwoDaysAgo[0], results.kwhsThreeDaysAgo[0], results.kwhsFourDaysAgo[0], results.kwhsFiveDaysAgo[0], results.kwhsSixDaysAgo[0], results.kwhsSevenDaysAgo[0]];
     inOrder[0].fromgrid = kwhsMeter - inOrder[0].fromgrid; //today
     inOrder[1].fromgrid = kwhsMeter - inOrder[1].fromgrid - inOrder[0].fromgrid; // day ago
     inOrder[2].fromgrid = kwhsMeter - inOrder[2].fromgrid - inOrder[1].fromgrid - inOrder[0].fromgrid;// 2 days ago
     inOrder[3].fromgrid = kwhsMeter - inOrder[3].fromgrid - inOrder[2].fromgrid - inOrder[1].fromgrid - inOrder[0].fromgrid;// 3 days ago
     inOrder[4].fromgrid = kwhsMeter - inOrder[4].fromgrid - inOrder[3].fromgrid - inOrder[2].fromgrid - inOrder[1].fromgrid - inOrder[0].fromgrid;// 4 days ago
     inOrder[5].fromgrid = kwhsMeter - inOrder[5].fromgrid - inOrder[4].fromgrid - inOrder[3].fromgrid - inOrder[2].fromgrid - inOrder[1].fromgrid - inOrder[0].fromgrid// 5 days ago
     inOrder[6].fromgrid = kwhsMeter - inOrder[6].fromgrid - inOrder[5].fromgrid - inOrder[4].fromgrid - inOrder[3].fromgrid - inOrder[2].fromgrid - inOrder[1].fromgrid - inOrder[0].fromgrid;// 6 days ago
     inOrder[7].fromgrid = kwhsMeter - inOrder[7].fromgrid - inOrder[6].fromgrid - inOrder[5].fromgrid - inOrder[4].fromgrid - inOrder[3].fromgrid - inOrder[2].fromgrid - inOrder[1].fromgrid - inOrder[0].fromgrid;// 7 days ago
     
     console.log(kwhsMeter);

function convertTimeStamp(data){
       var weeklyEnergy = [];

  data.forEach(function(element){
    //weeklyEnergy.className = "time";
    weeklyEnergy.push({date: moment(element._id).format("ddd MMM Do, HH:mm:ss"), energy: element.fromgrid / 1000, cost: parseFloat((element.fromgrid / 1000 * 0.0797).toFixed(2))});             
    //var totalEnergy = 0;
    //$.each(arr,function() {
    //total += this;
    //});
    //weeklyEnergy.push(
  })
   return weeklyEnergy;
}

var weeklyEnergy = convertTimeStamp(inOrder);

var total = {date: 'Total', energy: 0, cost: 0};
weeklyEnergy.forEach(
    function (value){ 
    total.energy += value.energy;
    total.cost += value.cost;
    }
);  

total.energy = total.energy.toFixed(3);
total.cost   = total.cost.toFixed(2);

weeklyEnergy.push(total);

console.log(weeklyEnergy);
    res.json(weeklyEnergy); // return all in JSON format

    });

});
       
    httpsRouter.get('/history/thismonth', function(req, res) {

          var kwhsMonthly = {

    kwhsNow: function (callback) {
        kwhsNowQuery = eagleData.eagleKwhs.find(),
          kwhsNowQuery.sort('-_id');
          kwhsNowQuery.limit(1);
          kwhsNowQuery.exec(function(err, data) {
            // if there is an error retrieving, send the error. nothing after res.send(err) will execute
            if (err)
                return callback(err)
            //console.log('Now: ',data[0].fromgrid);    
            callback(null, data);
        });
    }, 

    kwhsThisMonth: function (callback) {
        var thisMonth = moment().days(0).hours(0).minutes(0).seconds(0).format('x');

        var thisMonthQuery = eagleData.eagleKwhs.find();
          thisMonthQuery.where('_id').gte(thisMonth - 5000).lte(thisMonth + 5000); 
          thisMonthQuery.limit(1);
          thisMonthQuery.exec(function(err, data) {
            // if there is an error retrieving, send the error. nothing after res.send(err) will execute
            if (err)
                return callback(err);

            //console.log('Since today: ',data[0].fromgrid);  
            callback (null, data);

        });
     }
 };

async.parallel (kwhsMonthly, function (err, results) {

    if (err)
        throw err;

    console.log(results);
    res.json(results);   

});

    });


    httpsRouter.get('/api/demand', function(req, res) {

          var query = eagleData.eagleDemand.find();
          query.sort('-_id');
          query.limit(65);
          //query.sort('-_id');
          query.exec(function(err, data) {	

            // if there is an error retrieving, send the error. nothing after res.send(err) will execute
            if (err)
                res.send(err)
            //data.fromgrid = data.fromgrid[0] - data.fromgrid[data.fromgrid.length];
            res.json(data); 
            console.log(data);// return all in JSON format
        });
    });

/////////////////////////////////////LOGIN/LOGOUT HANDLERS////////////////////////

var user = {email: 'test@t',
            password: 't'};


    httpsRouter.post('/login', authenticate, function(req, res){
      var token = jwt.sign({email: user.email}, jwtSecret, {expiresInMinutes: 60});

      res.send({token: token, user: user.email});


    });

    httpsRouter.get('/user', function(req, res){
      
      res.send(req.user);

    });


function authenticate(req, res, next) {
  var body = req.body;
  console.log(body);
  if (!body.email || !body.password) {
    res.status(400).end('Must provide username or password');
  }
  if (body.email !== user.email || body.password !== user.password) {
    res.status(401).end('Username or password incorrect');
  }
  next();
}


module.exports = httpsRouter;