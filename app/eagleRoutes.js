// app/eagleRoutes.js
var express     = require('express'),
    xmlparser   = require('express-xml-bodyparser'),
    eagleData   = require('./models/eagleSchema'),
    util        = require('util'),
    moment      = require('moment');
    //io          = require('./socket');
    




    httpRouter  = express.Router();


     httpRouter.use(xmlparser());

     httpRouter.use(function(req, res, next){
          //console.log(moment().format("ddd MMM Do YY, h:mm:ss"));
          //console.log(util.inspect(req.body, false, null));
          next();
     });
     
     httpRouter.get('/', function(req, res, next){
     res.redirect('https://flirky.com:3002');
        
     });

var blockPriceDetail          = {devicemacid: '',
                                metermacid: '',
                                timestamp: '',
                                currentstart: '', //current bill start date
                                currentduration: '', //time past since start date
                                blockperiodconsumption: '', //kwhs used since start date
                                blockperiodconsumptionmultiplier: '',
                                blockperiodconsumptiondivisor: '',
                                numberofblocks: '',
                                multiplier: '',
                                divisor: '',
                                currency: '',
                                trailingdigits: '',
                                lowprice: '', //7.97 cents
                                kwhsallowance: '', //allowance 1309 kwhs at 7.97
                                highprice: ''}; //11.95 cents}

var currentSummationDelivered = {devicemacid: '',
                                metermacid: '',
                                timestamp: '',
                                summationdelivered: '',
                                summationreceived: '',
                                multiplier: '',
                                divisor: '',
                                digitsright: '',
                                digitsleft: '',
                                suppressleadingzero: ''};

     var   deviceInfo =         {devicemacid: '',
                                installcode: '',
                                linkkey: '',
                                fwversion: '',
                                hwversion: '',
                                imagetype: '',
                                manufacturer: '',
                                modelid: '',
                                datecode: '',
                                port: ''};

     var   instantaneousDemand,
        messageCluster,
        meterInfo,
        networkInfo,
        priceCluster,
        scheduleInfo,
        timeCluster,
        fastPollStatus;


     var pricePerKwh;

     httpRouter.post('/eagleupload', function(req, res, next){
	     //BlockPriceDetail
          if (req.body.rainforest.blockpricedetail !== undefined){   
             //Current bill start date calc
             blockPriceDetail.devicemacid                      = parseInt(req.body.rainforest.blockpricedetail[0].devicemacid, 16);
             blockPriceDetail.metermacid                       = parseInt(req.body.rainforest.blockpricedetail[0].metermacid, 16);
             blockPriceDetail.timestamp                        = parseInt(req.body.rainforest.blockpricedetail[0].timestamp, 16);
             blockPriceDetail.currentstart                     = parseInt(req.body.rainforest.blockpricedetail[0].currentstart, 16); //current bill start date
             blockPriceDetail.currentduration                  = parseInt(req.body.rainforest.blockpricedetail[0].currentduration, 16); //time past since start date
             blockPriceDetail.blockperiodconsumption           = parseInt(req.body.rainforest.blockpricedetail[0].blockperiodconsumption, 16); //kwhs used since start date
             blockPriceDetail.blockperiodconsumptionmultiplier = parseInt(req.body.rainforest.blockpricedetail[0].blockperiodconsumptionmultiplier, 16);
             blockPriceDetail.blockperiodconsumptiondivisor    = parseInt(req.body.rainforest.blockpricedetail[0].blockperiodconsumptiondivisor, 16);
             blockPriceDetail.numberofblocks                   = parseInt(req.body.rainforest.blockpricedetail[0].numberofblocks, 16);
             blockPriceDetail.multiplier                       = parseInt(req.body.rainforest.blockpricedetail[0].multiplier, 16);
             blockPriceDetail.divisor                          = parseInt(req.body.rainforest.blockpricedetail[0].divisor, 16);
             blockPriceDetail.currency                         = parseInt(req.body.rainforest.blockpricedetail[0].currency, 16);
             blockPriceDetail.trailingdigits                   = parseInt(req.body.rainforest.blockpricedetail[0].trailingdigits, 16);
             blockPriceDetail.lowprice                         = parseInt(req.body.rainforest.blockpricedetail[0].price1, 16); //7.97 cents
             blockPriceDetail.kwhsallowance                    = parseInt(req.body.rainforest.blockpricedetail[0].threshold1, 16); //allowance 1309 kwhs at 7.97
             blockPriceDetail.highprice                        = parseInt(req.body.rainforest.blockpricedetail[0].price2, 16); //11.95 cents

          
          var billPeriodStart = formatDate(blockPriceDetail.currentstart);
          var billPeriodLength = formatDate(blockPriceDetail.currentstart + blockPriceDetail.currentduration);
          //var billPeriodStartDate = parseInt(req.body.rainforest.blockpricedetail[0].currentstart, 16);
          //billPeriodStartDate = (billPeriodStartDate + 946684800) * 1000;
          //billPeriodStartDateString = new Date(billPeriodStartDate);
          //billPeriodStartDateString = billPeriodStartDateString.toLocaleDateString();
          //Current duration
          var currentDurationDecimal = parseInt(req.body.rainforest.blockpricedetail[0].currentduration, 16);
          //Current Kwhs used since bill start date
          var currentKwhPeriod = parseInt(req.body.rainforest.blockpricedetail[0].blockperiodconsumption, 16);
          currentKwhPeriod = (currentKwhPeriod / 1000).toFixed(1);
          //No of Kwh's quota charged at $7.52 1309Kwhs until higher rate of $11.27
          var lowRateQuotaKwh = parseInt(req.body.rainforest.blockpricedetail[0].threshold1, 16);
          //Calculate remaining Kwh's quota
          var kwhsQuotaRemaining = (lowRateQuotaKwh - currentKwhPeriod).toFixed(1);
          //Calculate percentage of quota used
          var percentageQuota = ((currentKwhPeriod / lowRateQuotaKwh) * 100).toFixed(1);
            //Calculate Bill if on higher rate    
                    if (currentKwhPeriod > lowRateQuotaKwh){
                         var highRate    = ((currentKwhPeriod - lowRateQuotaKwh) * pricePerKwh) / 100;
                         var  billEstimate = (lowRateQuotaKwh * 0.0752) + highRate;
                         }
             else if (currentKwhPeriod <= lowRateQuotaKwh){
                              billEstimate = (currentKwhPeriod * 0.0752);
                        }
            var billTax = Math.round((billEstimate / 100) * 5);
            billEstimate = (billEstimate + billTax).toFixed(2);  
          //Send to socket
          io.emit("billEstimate", billEstimate);
          io.emit("kwhsQuotaRemaining", kwhsQuotaRemaining);
          io.emit("percentageQuota", percentageQuota);
          io.emit("billPeriodStartDate", billPeriodStart);
          io.emit("currentDurationDecimal", currentDurationDecimal);
          io.emit("currentKwhPeriod", currentKwhPeriod);
          io.emit("lowRateQuotaKwh", lowRateQuotaKwh);
          

          //console.log(blockPriceDetail);       
          res.sendStatus(200);
    }
        
      

      //CurrentSummationDelivered
      else if (req.body.rainforest.currentsummationdelivered !== undefined){
          
         currentSummationDelivered.devicemacid         = parseInt(req.body.rainforest.currentsummationdelivered[0].devicemacid, 16);
         currentSummationDelivered.metermacid          = parseInt(req.body.rainforest.currentsummationdelivered[0].metermacid, 16);
         currentSummationDelivered.timestamp           = parseInt(req.body.rainforest.currentsummationdelivered[0].timestamp, 16);
         currentSummationDelivered.summationdelivered  = parseInt(req.body.rainforest.currentsummationdelivered[0].summationdelivered, 16);
         currentSummationDelivered.summationreceived   = parseInt(req.body.rainforest.currentsummationdelivered[0].summationreceived, 16);
         currentSummationDelivered.multiplier          = parseInt(req.body.rainforest.currentsummationdelivered[0].multiplier, 16);
         currentSummationDelivered.divisor             = parseInt(req.body.rainforest.currentsummationdelivered[0].divisor, 16);
         currentSummationDelivered.digitsright         = parseInt(req.body.rainforest.currentsummationdelivered[0].digitsright, 16);
         currentSummationDelivered.digitsleft          = parseInt(req.body.rainforest.currentsummationdelivered[0].digitsright, 16);
         currentSummationDelivered.suppressleadingzero = req.body.rainforest.currentsummationdelivered[0].suppressleadingzero;

          var kwhsConvert = (currentSummationDelivered.summationdelivered / 1000).toFixed(1);
          io.emit("meterReading", kwhsConvert);

          //Save post data to MongoDB //////////////////////
          var eagleKwhsUsed = new eagleData.eagleKwhs({
            _id: formatDate(currentSummationDelivered.timestamp),
            fromgrid: currentSummationDelivered.summationdelivered
          });

          eagleKwhsUsed.save(function(err){
            if(err)
                console.log(err);
            //else
                //console.log(eagleKwhsUsed);
          });
          //////////////////////////////////////////////////
          //console.log(currentSummationDelivered);
          res.sendStatus(200);
          
        
      
     }
      //Post DeviceInfo to Mysql    
     else if (req.body.rainforest.deviceinfo !== undefined){
          
         deviceInfo.devicemacid  = parseInt(req.body.rainforest.deviceinfo[0].devicemacid, 16);
         deviceInfo.installcode  = req.body.rainforest.deviceinfo[0].installcode;
         deviceInfo.linkkey      = req.body.rainforest.deviceinfo[0].linkkey;
         deviceInfo.fwversion    = req.body.rainforest.deviceinfo[0].fwversion;
         deviceInfo.hwversion    = req.body.rainforest.deviceinfo[0].hwversion;
         deviceInfo.imagetype    = parseInt(req.body.rainforest.deviceinfo[0].imagetype, 16);
         deviceInfo.manufacturer = req.body.rainforest.deviceinfo[0].manufacturer;
         deviceInfo.modelid      = req.body.rainforest.deviceinfo[0].modelid;
         deviceInfo.datecode     = req.body.rainforest.deviceinfo[0].datecode;
         deviceInfo.port         = req.body.rainforest.deviceinfo[0].port;
  
       
          //console.log(deviceInfo);
          res.sendStatus(200);
         
        
      
      }
      //Post InstantaneousDemand to Mysql
      else if (req.body.rainforest.instantaneousdemand !== undefined){
       
       instantaneousDemand =       {devicemacid: parseInt(req.body.rainforest.instantaneousdemand[0].devicemacid, 16),
                                       metermacid: parseInt(req.body.rainforest.instantaneousdemand[0].metermacid, 16),
                                       timestamp: parseInt(req.body.rainforest.instantaneousdemand[0].timestamp, 16),
                                       demand: parseInt(req.body.rainforest.instantaneousdemand[0].demand, 16),
                                       multiplier: parseInt(req.body.rainforest.instantaneousdemand[0].multiplier, 16),
                                       divisor: parseInt(req.body.rainforest.instantaneousdemand[0].divisor, 16),
                                       digitsright: parseInt(req.body.rainforest.instantaneousdemand[0].digitsright, 16),
                                       digitsleft: parseInt(req.body.rainforest.instantaneousdemand[0].digitsleft, 16),
                                       suppressleadingzero: req.body.rainforest.instantaneousdemand[0].suppressleadingzero };

             
          //Send data to client socket////////////////////////////////////////////////////////////////////////////////// 
           //Current demand in Watts
          /*if (instantaneousDemand.demand.length >= 4){
                instantaneousDemand.demand = instantaneousDemand.demand + ",";
              } */ 
          io.emit("demand", instantaneousDemand.demand); 
          io.emit("timeWatts", [formatDate(instantaneousDemand.timestamp), instantaneousDemand.demand]);
          var amps = instantaneousDemand.demand / 241.5; //Voltage measured
          amps = Math.round(amps * 100) / 100;
          io.emit('current', amps);
          //Time current demand was taken 
          var demandCurrentDate = (946684800 + instantaneousDemand.timestamp) * 1000;
          //Get unix time in millis
          var d = (new Date).getTime(); 
          var demandTimeDiff = (d - demandCurrentDate) / 1000;
          demandTimeDiff = Math.round(demandTimeDiff);
          //Calculate Cost per hour
          var costPerHour = ((instantaneousDemand.demand / 1000) * pricePerKwh).toFixed(0);
          io.emit("costPerHour", costPerHour);
          io.emit("demandTimeStamp", demandTimeDiff);
          
          //Save post data to MongoDB //////////////////////
          var eagleDemand = new eagleData.eagleDemand({
            _id: formatDate(instantaneousDemand.timestamp),
            demand: instantaneousDemand.demand
          });

          eagleDemand.save(function(err) {
              if (err) throw err;

              //console.log('Demand saved successfully!');
              //console.log(eagleDemand);
          });
          //////////////////////////////////////////////////
          //console.log(instantaneousDemand);
          res.sendStatus(200);
          
        
      
      }
      //messagecluster
      else if (req.body.rainforest.messagecluster !== undefined){
           messageCluster =      {devicemacid: parseInt(req.body.rainforest.messagecluster[0].devicemacid, 16),
                                     metermacid: parseInt(req.body.rainforest.messagecluster[0].metermacid, 16),
                                     timestamp: parseInt(req.body.rainforest.messagecluster[0].timestamp, 16),
                                     id: parseInt(req.body.rainforest.messagecluster[0].id, 16),
                                     text: parseInt(req.body.rainforest.messagecluster[0].text, 16),
                                     priority: parseInt(req.body.rainforest.messagecluster[0].priority, 16),
                                     starttime: parseInt(req.body.rainforest.messagecluster[0].starttime, 16),
                                     duration: parseInt(req.body.rainforest.messagecluster[0].duration, 16),
                                     confirmationrequired: req.body.rainforest.messagecluster[0].confirmationrequired,
                                     confirmed: req.body.rainforest.messagecluster[0].confirmed,
                                     queue: req.body.rainforest.messagecluster[0].queue };

          //console.log(messageCluster);
          res.sendStatus(200);    
      
      }
      //Post meterinfo to Mysql
      else if (req.body.rainforest.meterinfo !== undefined){
           meterInfo =           {devicemacid: parseInt(req.body.rainforest.meterinfo[0].devicemacid, 16),
                                     metermacid: parseInt(req.body.rainforest.meterinfo[0].metermacid, 16),
                                     type: parseInt(req.body.rainforest.meterinfo[0].type, 16),
                                     nickname: req.body.rainforest.meterinfo[0].nickname,
                                     account: req.body.rainforest.meterinfo[0].account,
                                     auth: req.body.rainforest.meterinfo[0].auth,
                                     host: req.body.rainforest.meterinfo[0].host,
                                     enabled: req.body.rainforest.meterinfo[0].enabled };
       
          //console.log(meterInfo);
          res.sendStatus(200);
      }
      //Post networkinfo to Mysql
     else if (req.body.rainforest.networkinfo !== undefined){
           networkInfo =         {devicemacid: parseInt(req.body.rainforest.networkinfo[0].devicemacid, 16),
                                     coordmacid: parseInt(req.body.rainforest.networkinfo[0].coordmacid, 16),
                                     status: req.body.rainforest.networkinfo[0].status,
                                     description: req.body.rainforest.networkinfo[0].description,
                                     extpanid: parseInt(req.body.rainforest.networkinfo[0].extpanid, 16),
                                     channel: req.body.rainforest.networkinfo[0].channel,
                                     shortaddr: parseInt(req.body.rainforest.networkinfo[0].shortaddr, 16),
                                     linkstrength: parseInt(req.body.rainforest.networkinfo[0].linkstrength, 16) };

          //Send to socket
          io.emit("meterStatus", networkInfo.status);
          io.emit("meterChannel", networkInfo.channel);
          io.emit("linkStrength", networkInfo.linkstrength);
          //console.log(networkInfo);
          res.sendStatus(200);
         
     }
     //Post pricecluster
     else if (req.body.rainforest.pricecluster !== undefined){
           priceCluster =      {devicemacid: parseInt(req.body.rainforest.pricecluster[0].devicemacid, 16),
                                   metermacid: parseInt(req.body.rainforest.pricecluster[0].metermacid, 16),
                                   timestamp: parseInt(req.body.rainforest.pricecluster[0].timestamp, 16),
                                   price: parseInt(req.body.rainforest.pricecluster[0].price, 16),
                                   currency: parseInt(req.body.rainforest.pricecluster[0].currency, 16),
                                   trailingdigits: parseInt(req.body.rainforest.pricecluster[0].trailingdigits, 16),
                                   tier: parseInt(req.body.rainforest.pricecluster[0].tier, 16),
                                   starttime: parseInt(req.body.rainforest.pricecluster[0].starttime, 16),
                                   duration: parseInt(req.body.rainforest.pricecluster[0].duration, 16),
                                   ratelabel: req.body.rainforest.pricecluster[0].ratelabel };

       
          //Current cost per Kwh
            pricePerKwh = priceCluster.price;
            pricePerKwh = pricePerKwh / 100;
          io.emit("pricePerKwh", pricePerKwh);
          //console.log(priceCluster);
          res.sendStatus(200);
    
     }
     //Post scheduleinfo
     else if (req.body.rainforest.scheduleinfo !== undefined){
           scheduleInfo =      {devicemacid: parseInt(req.body.rainforest.scheduleinfo[0].devicemacid, 16),
                                   metermacid: parseInt(req.body.rainforest.scheduleinfo[0].devicemacid, 16),
                                   mode: req.body.rainforest.scheduleinfo[0].mode,
                                   event: req.body.rainforest.scheduleinfo[0].event,
                                   frequency: parseInt(req.body.rainforest.scheduleinfo[0].frequency, 16),
                                   enabled: req.body.rainforest.scheduleinfo[0].enabled };

       
          //console.log(scheduleInfo);
          res.sendStatus(200);
    
     }
     //Post timecluster 
     else if (req.body.rainforest.timecluster !== undefined){
           timeCluster =       {devicemacid: parseInt(req.body.rainforest.timecluster[0].devicemacid, 16),
                                   metermacid: parseInt(req.body.rainforest.timecluster[0].metermacid, 16),
                                   utctime: parseInt(req.body.rainforest.timecluster[0].utctime, 16),
                                   localtime: parseInt(req.body.rainforest.timecluster[0].localtime, 16) };

       
          //console.log(timeCluster);
          res.sendStatus(200);
    
      
     }
     //Post fastpollstatus
     else if (req.body.rainforest.fastpollstatus !== undefined){
           fastPollStatus =   {devicemacid: parseInt(req.body.rainforest.fastpollstatus[0].devicemacid, 16),
                                  metermacid: parseInt(req.body.rainforest.fastpollstatus[0].metermacid, 16),
                                  frequency: parseInt(req.body.rainforest.fastpollstatus[0].frequency, 16),
                                  endtime: parseInt(req.body.rainforest.fastpollstatus[0].endtime, 16) };

          //console.log(fastPollStatus);
          res.sendStatus(200);
     }
     //Display anything else 
     else {res.sendStatus(200);
        console.log(util.inspect(req.body, false, null));
        console.log("200 OK sent");
     }


        
     });
// Time from 01-01-1970 to 01-01-2000 = 946684800 seconds
// Subtract 10800 seconds (3 hours) to convert to PST = 946674000
function formatDate(data){
    var result = (data + 946684800) * 1000;
    /*var d = new Date((data + 946684800) * 1000),
        year    = d.getFullYear(),
        month   = d.getMonth();
        month++;
    var day     = d.getDate(),
        hours   = d.getHours(),
        minutes = d.getMinutes();
        minutes = minutes + "";
        if (minutes.length == 1){
          minutes = "0" + minutes;}
      var seconds = d.getSeconds();
          seconds = seconds + "";
        if (seconds.length == 1){
          seconds = "0" + seconds;}
      var result = year + "-" + month + "-" + day + " " + hours + ":" + minutes + ":" + seconds; */
    return result;
};


module.exports.httpRouter                = httpRouter;
module.exports.blockPriceDetail          = blockPriceDetail;
module.exports.currentSummationDelivered = currentSummationDelivered;
module.exports.deviceInfo                = deviceInfo;
module.exports.instantaneousDemand       = instantaneousDemand;
module.exports.messageCluster            = messageCluster;
module.exports.meterInfo                 = meterInfo;
module.exports.networkInfo               = networkInfo;
module.exports.priceCluster              = priceCluster;
module.exports.scheduleInfo              = scheduleInfo;
module.exports.timeCluster               = timeCluster;
module.exports.fastPollStatus            = fastPollStatus;



