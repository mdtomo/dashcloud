// app/socket.js



var socketIO		   = require('socket.io');
var eaglePosts 		   = require('./eagleRoutes');
var moment      	   = require('moment');



module.exports.listen = function(appSSL){
io = socketIO.listen(appSSL)
//Socket IO from Client
io.on('connection', function(socket){
	var socketId = socket.id;
    var clientIp = socket.request.connection.remoteAddress;
	    console.log('Client Connected IP:',clientIp, 'SocketID:', socketId, '@', moment().format());
		socket.emit('serverconn', 'Connected to server');
		socket.on('clientconn', function(data){
		console.log(data);
		});
		socket.on('disconnect', function(){
		console.log('Client Disconnected IP:',clientIp, 'SocketID:', socketId, '@', moment().format());
		});
		
});

		io.emit('gateway', eaglePosts.currentSummationDelivered.timestamp);
		//io.emit("demand", eaglePosts.instantaneousDemand.demand); 

}
