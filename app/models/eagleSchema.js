// app/models/eagleSchema.js
var mongoose = require('mongoose'),
	Schema 	 = mongoose.Schema;


var userSchema = new mongoose.Schema({
 	name: String,
 	username: { type: String, required: true, unique: true },
  	password: { type: String, required: true },
  	admin: Boolean,
  	created_at: Date,
  	updated_at: Date
});

var eagleDemandSchema = new mongoose.Schema({
	_id: Number,
	demand: Number},
  { versionKey: false});

var eagleKwhConsu = new mongoose.Schema({
	_id: Number,
	fromgrid: Number},
  { versionKey: false});

// Mongoose also creates a MongoDB collection called 'Movies' for these documents.
var user = mongoose.model('user', userSchema);
var eagleDemand = mongoose.model('eagleDemand', eagleDemandSchema);
var eagleKwhs = mongoose.model('eagleKwhs', eagleKwhConsu);

module.exports.user = user;
module.exports.eagleDemand = eagleDemand;
module.exports.eagleKwhs = eagleKwhs;