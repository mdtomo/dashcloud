// app/demand/demandCtrl.js
angular.module('demandServiceMod', []) 

//each function returns a promise object
.service('demandService', function($http) {
        
        var model = this,
        	url = {fetch: '/api/demand'},
        	//model.demand = 'from demand service';
            demand;
      
        function extract(result){
            return result.data;
        }    
        
        function getLatestDemand(result){
            demand = extract(result);
            return demand;
        }

        model.getdemand = function() {
        	  
        	  return $http.get(url.fetch).then(getLatestDemand);
                        
		
        };			
                    


        
 		
});