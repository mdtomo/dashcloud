// app/demand/demandChartDirective.js
angular.module('demandChartDirectiveMod', []) 

	.directive('chart', function() {
    	
var options = {
    series: {
        hoverable: true,
        shadowSize: 2,
        lines: {
            show: true,
            lineWidth: 1.2,
            fill: true
        }
    },
    xaxis: {
        mode: "time",
        timezone: "browser",
        tickSize: [5, "second"],
        tickFormatter: "", 
        axisLabel: "Time",
        axisLabelUseCanvas: true,
        axisLabelFontSizePixels: 12,
        axisLabelFontFamily: 'Verdana, Arial',
        axisLabelPadding: 10,
        axisLabelColour: "#474747"
    },
    yaxis: {
       // min: 0,
       // max: 10000, 
        autoscaleMargin: 1,       
        tickSize: 500,
        tickFormatter: "",
        axisLabel: "Demand",
        axisLabelUseCanvas: true,
        axisLabelFontSizePixels: 12,
        axisLabelFontFamily: 'Verdana, Arial',
        axisLabelPadding: 10,
        axisLabelColour: "#474747"
    },
    legend: {  
        show: true,      
        labelBoxBorderColor: "#fff",
        position: "ne"
    },
    grid: {                
        backgroundColor: "#191919",
        tickColor: "#333333"
    }
};


        return {
        restrict: 'E',
        link: function(scope, elem, attrs) {

             var demandChart = null;
                
                   
            scope.$watch(attrs.ngModel, function(v){
                if(!demandChart){
                    demandChart = $.plot(elem, v , options);
                    elem.show();
                    console.log('directive:', demandChart);
                }else{
                    demandChart.setData(v);
                    demandChart.setupGrid();
                    demandChart.draw();

            /*
            var demandChart = scope[attrs.ngModel];
            $.plot(elem, demandChart, options);
            elem.show();
        	*/
                }
        	});
    	}
    };
});
