 // app/demand/demandCtrl.js
angular.module('demandControllerMod', []) 

 
 .controller('demandController', function($stateParams, demandService, $socket, $scope) {       
 	var demand = this;
 	demand.demand = $stateParams.demand;
    demand.tooltip = "tool tip!";
    demand.status = 'animated fadeIn';
    demand.previousKwhs = 100;
    

    demandService.getdemand()
        .then(function(result){
            demand.data = result;
            demand.watts = demand.data[0].demand + 'W, ';
            demand.amps   = (demand.data[0].demand / 241.5).toFixed(2) + 'A';
        });
        


         $socket.on('serverconn', function (data) { 
                demand.serverConn = data;
                console.log('socket:' + data);
            });
         $socket.on('demand', function(data){
                demand.watts = data + 'W, ';
                console.log(demand.previousKwhs, data);
                if (demand.previousKwhs < data){
                	demand.status = 'animated fadeIn';
                	demand.status = 'animated fadeInRed';
                } else if(demand.previousKwhs > data){
                	demand.status = 'animated fadeIn';
                	demand.status = 'animated fadeInGreen';
                } else {
                	demand.status = 'animated fadeIn';
                }
                demand.previousKwhs = data;	
                console.log(demand.status);
            });

         $socket.on('current', function(data){
                demand.amps = data + 'A';
            }); 

         $socket.on('costPerHour', function(data){
                demand.cost = data + '¢ per hour';
            });

         $socket.on('gateway', function(data){
                demand.gateway = data;
            });   
            
        
        
 })

/*.directive('fader', function($scope) {
   $scope.$watch(function(scope) { return scope.demand.watts },
              function(newValue, oldValue) {
                 console.log('test'); 
              }
             );
})*/;