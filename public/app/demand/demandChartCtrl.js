// app/demand/demandChartController.js
angular.module('demandChartControllerMod', []) 

.controller('demandChartController', function($scope, $socket, demandService) {
	//var chart = this;
 	//	chart.demand = $stateParams.chart;
 	//var test2 = [[[1427341420000, 1000], [1427341420500, 500], [1427341429000, 2000],[1427341430000, 3000],[1427341431000, 1000]]];
	//console.log('demandChartController test:', test2);
	var array = $scope.demandChart;
	
    $scope.options =  {
    series: {
        curvedLines: {
             apply: true,
             active: true,
             monotonicFit: true
        },
        color: "#00AD07",  //, "#41ba2f", "#007FFF", "#dba255", "#919733"],
        lines: {
            show: true,
            fill: true,
            lineWidth: 2,
            fillColor: "rgba(71, 228, 83, 0.5)"
            
        },
        points: {
            show: true,
            fill: true,
            radius: 2,
            fillColor: "rgba(71, 228, 83, 0.5)"
        }
        
        
    },
    xaxis: {
        //transform: function (v) { return -v; },
        mode: "time",
        timezone: "browser",
        tickSize: [1, "minute"],
        axisLabel: "Time",
        axisLabelUseCanvas: true,
        axisLabelFontSizePixels: 12,
        axisLabelFontFamily: 'Verdana, Arial',
        axisLabelPadding: 10,
        axisLabelColour: "#474747"
    },
    yaxis: {
       // min: 0,
       // max: 10000, 
        autoscaleMargin: 0.05,       
        tickSize: 1000,
        axisLabel: "Demand (W)",
        axisLabelUseCanvas: true,
        axisLabelFontSizePixels: 12,
        axisLabelFontFamily: 'Verdana, Arial',
        axisLabelPadding: 10,
        axisLabelColour: "#474747"
    },
    legend: {  
        show: true,      
        labelBoxBorderColor: "#fff",
        position: "ne"
    },
    grid: {    
        hoverable: true,
        backgroundColor: "#808080",            
        borderWidth: 0
    }
};

        var arrayed;
 

 		demandService.getdemand()
        .then(function(result){
        		arrayed = [];
            	for (var i = 0; i < result.length; i++){
            		arrayed.push([result[i]._id, result[i].demand] );
            	}
            	arrayed = [arrayed]; 
                $scope.demandChart = arrayed;
                //$scope.test = arrayed;
                //console.log('demandChartController:', $scope.demandChart);

        });

        $socket.on('timeWatts', function(data){
                arrayed[0].unshift(data);
                arrayed[0].pop();
                //console.log('socket: ', $scope.demandChart) 
            }); 

  
/*$scope.resizeContainer(".resizeContainer").resizable({
                    maxWidth: 900,
                    maxHeight: 500,
                    minWidth: 450,
                    minHeight: 250
    });*/
 		


});