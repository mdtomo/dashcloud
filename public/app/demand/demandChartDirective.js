// app/demand/demandChartDirective.js
angular.module('demandChartDirectiveMod', []) 

.directive('chart', function() {
  return {
    restrict: 'EA',
    template: '<div></div>',
    scope: {
      dataset: '=',
      options: '=',
      callback: '='
    },
    link: function(scope, element, attributes) {
      var height, init, onDatasetChanged, onOptionsChanged, plot, plotArea, width, _ref, _ref1;
      plot = null;
      width = attributes.width || '100%';
      height = attributes.height || '100%';
      if (((_ref = scope.options) != null ? (_ref1 = _ref.legend) != null ? _ref1.container : void 0 : void 0) instanceof jQuery) {
        throw 'Please use a jQuery expression string with the "legend.container" option.';
      }
      if (!scope.dataset) {
        scope.dataset = [];
      }
      if (!scope.options) {
        scope.options = {
          legend: {
            show: false
          }
        };
      }
      plotArea = $(element.children()[0]);
      plotArea.css({
        width: width,
        height: height
      });
      init = function() {
        var plotObj; //To activate curved lines. [{data: d1, lines: { show: true}, curvedLines: {apply: true}}]
        plotObj = $.plot(plotArea, scope.dataset, scope.options);
                plotArea.bind("plothover", function (event, pos, item) {
                    //alert("You clicked at " + pos.x + ", " + pos.y);
                    // axis coordinates for other axes, if present, are in pos.x2, pos.x3, ...
                    // if you need global screen coordinates, they are pos.pageX, pos.pageY

                      if (item) {
                                //alert(pos.x + ", " + pos.y);
                                 $("#tooltip").remove();
                                  var x = item.datapoint[0].toFixed(2),
                                      y = item.datapoint[1].toFixed(2);
              
                              showTooltip(item.pageX, item.pageY, String(Math.floor(parseFloat(y))) + " W");
                      }
                      else {
                                 $("#tooltip").remove();
                                //  previousPoint = null;            
                            }
                })

                  

        if (scope.callback) {
          scope.callback(plotObj);
        }
        return plotObj;
      };

      onDatasetChanged = function(dataset) {
        if (plot) {
          plot.setData(dataset);
          plot.setupGrid();
          return plot.draw();
        } else {
          return plot = init();
        }
      };

      scope.$watch('dataset', onDatasetChanged, true);

      onOptionsChanged = function() {
        return plot = init();
      };
      return scope.$watch('options', onOptionsChanged, true);
    }
  };

function showTooltip(x, y, contents) {
      $('<div id="tooltip">' + contents + '</div>').css( {
          position: 'absolute',
          display: 'none',
          top: y - 35,
          left: x - 40,
          border: '1px solid #fdd',
          padding: '1px',
          color: '#fff',
          'background-color': '#000',
          opacity: 0.80
      }).appendTo("body").fadeIn(0);
      $("#tooltip").css('font-size','16px');
      $("#tooltip").css('background-color','#000');
      $("#tooltip").css('padding','5px');
      $("#tooltip").css('border','1px solid #ccc');
  }


});
