
angular.module('historyServiceMod', [])

//each function returns a promise object
.service('historyService', function($http) {

        var model = this,
            url = {fetch: '/api/history/7day'},
            //model.demand = 'from demand service';
            history;
      
        function extract(result){
            return result.data;

        }    
        
        function getLatestHistory(result){
            history = extract(result);
            return history;
        }

        model.get24hrsHistory = function() {
              
              return $http.get(url.fetch).then(getLatestHistory);
                        
        
        };


});