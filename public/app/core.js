// js/core.js
angular.module('dashapp', [
	'ui.router',
    'ui.bootstrap',
    'ngAnimate',
    //'curvedLines',
    'demandControllerMod',
    'historyControllerMod',
	'demandServiceMod',
    'historyServiceMod',
    'demandChartControllerMod',
    'demandChartDirectiveMod',
    'socketServiceMod',
    'socketControllerMod',
    'loginControllerMod',
    'loginServiceMod',
    'authServiceMod',
    'interceptServiceMod',
    //'indexControllerMod'
    'loginNavControllerMod',
    'demandNavControllerMod',
    'historyNavControllerMod'
	])

.config(function($stateProvider, $urlRouterProvider, $socketProvider, $httpProvider) {
     
    $stateProvider
    .state('index', {
        url: "/",
        views: {
             'navbar@': {templateUrl: 'app/nav/nav.html',
                          controller: 'loginNavController as nav'},
              '@': {templateUrl: 'app/login/loginView.html',
                    controller: 'loginController as auth'}
        }
                   
    })
    .state('history', {
        url: "/history",
        views: {
             'navbar@': {templateUrl: 'app/nav/nav.html',
                          controller: 'historyNavController as nav'},
              '@': {templateUrl: 'app/history/historyView.html',
                          controller: 'historyController as history'}
        }
    })    
    .state('demand', {
        url: "/demand",
        views: {
             'navbar@': {templateUrl: 'app/nav/nav.html',
                          controller: 'demandNavController as nav'},
           'demandLatest@': {templateUrl: 'app/demand/demandLatest.html',
                             controller: 'demandController as demand'},
           'demandChart@': {templateUrl: 'app/demand/demandChart.html',
                            controller: 'demandChartController'}                             
        }    

    });    
        
    $urlRouterProvider.otherwise('/');
    $socketProvider.setConnectionUrl('https://flirky.com:3002');
    $httpProvider.interceptors.push('interceptService');
    

});

