
angular.module('socketControllerMod', [])


.controller('socketController', function Ctrl($scope, $socket) {
            
            $socket.on('echo', function (data) {
                $scope.serverResponse = data;
            });
            
            $scope.emitBasic = function emitBasic() {
                console.log('echo event emited');
                $socket.emit('echo', $scope.dataToSend);
                $scope.dataToSend = '';
            };
         
            $scope.emitACK = function emitACK() {
                $socket.emit('echo-ack', $scope.dataToSend, function (data) {
                    
                    $scope.serverResponseACK = data;
                });
                $scope.dataToSend = '';
            };
        });