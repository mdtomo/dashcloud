angular.module('authServiceMod', [])

.service('authService', function authToken($window) {

        var store = $window.localStorage;
        var key = 'flirky-user';

        return{
        	getToken: getToken,
        	setToken: setToken
        };

        function getToken(){
        	return store.getItem(key);
        }

        function setToken(token){
        	if (token){
        		store.setItem(key, token);
        	} else {
        		store.removeItem(key);
        	}
        }


});