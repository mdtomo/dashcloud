
angular.module('loginServiceMod', [])

.service('loginService', function($http, authService, $q) {

        var model = this,
            url = {post: '/login', getUser: '/user'},
            login,
            user;
            model.loggedIn = 'Not logged in';

      
       // return {
       //     login: model.login,
       //     logout: model.logout
       // }

        model.login = function(email, password){
              
              return $http.post(url.post, {
                email: email,
                password: password
                }).then(function success(response){
                    authService.setToken(response.data.token);
                    return response;
                });
        } 

        model.logout = function(){
            authService.setToken();
            model.loggedIn = 'Logged out';
        }  

        cacheUser = function(result){
            user = result.data;
            return user;

        }

        model.getUser = function() {
            if (authService.getToken()) {
                model.loggedIn = 'Logged In';
                return (user) ? $q.when(user) : $http.get(url.getUser).then(cacheUser);
                



            
            } else {
            return $q.reject({ data: 'client has no auth token' });
            }
        }  


});