// app/login/loginCtrl.js
angular.module('loginControllerMod', []) 


.controller('loginController', function($state, $stateParams, loginService) {
       var auth = this;
           auth.login = login;

       function login(email, password){
     		loginService.login(email, password)	
     		.then(function success(response){
     			auth.user = response.data.user;
          console.log('User', auth.user);
          console.log('Token', response.data.token);
          $state.go('demand');
       		
          }, handleError);
       }		

       function handleError(response){
       		auth.error = (response.data);

       }  


       // initialization
       // loginService.getUser().then(function success(response) {
       // auth.user = response.email;
       // });     
});