angular.module('interceptServiceMod', [])

.service('interceptService', function (authService) {

        return {
                request: addToken
        };
        
        function addToken(config){
                var token = authService.getToken();
                if (token){
                        config.headers = config.headers || {};
                        config.headers.Authorization = 'Bearer ' + token;
                }
                return config;         
        } 
});