var socket = io('https://flirky.com');

socket.on('connecting', function(){
document.getElementById('socketstatus').innerHTML="Connecting";
document.getElementById('socketclass').className="alert alert-info alert-dismissible";
});

socket.on('connect', function(){
document.getElementById('socketstatus').innerHTML="Connected";
document.getElementById('socketclass').className="alert alert-success alert-dismissible";
});

socket.on('disconnect', function(){	
document.getElementById('socketstatus').innerHTML="Disconnected";
document.getElementById('socketclass').className="alert alert-danger alert-dismissible";
});

socket.on('reconnecting', function(){
document.getElementById('socketstatus').innerHTML="Reconnecting";
document.getElementById('socketclass').className="alert alert-warning alert-dismissible";
});

 socket.on('reconnect', function(){
document.getElementById('socketstatus').innerHTML="Reconnected";
document.getElementById('socketclass').className="alert alert-success alert-dismissible";
});

socket.on('connect_failed', function(){

});

socket.on('reconnect-failed', function(){

});

socket.on('serverconn', function(data){
	console.log(data);
	socket.emit('clientconn', 'Client Connected');
});
 
socket.on('demand', function(data){
	document.getElementById('demand').innerHTML="<h1>"+ data + "W</h1>" ;
});

socket.on('current', function(data){
	document.getElementById('current').innerHTML="<h1>"+ data + "A</h1>";
});

socket.on('pricePerKwh', function(data){
	document.getElementById('pricePerKwh').innerHTML=data + "c";
});

socket.on('costPerHour', function(data){
	document.getElementById('costPerHour').innerHTML=data + "c";
});

socket.on('meterReading', function(data){
	document.getElementById('meterReading').innerHTML=data +" KWh";
});

socket.on('billPeriodStartDate', function(data){
	document.getElementById('billPeriodStartDate').innerHTML="Bill Period  "+ data;
});

socket.on('lowRateQuotaKwh', function(data){
	document.getElementById('lowRateQuotaKwh').innerHTML=data +" KWh";
});

socket.on('currentKwhPeriod', function(data){
	document.getElementById('currentKwhPeriod').innerHTML=data +" KWh";
});

socket.on('kwhsQuotaRemaining', function(data){
	document.getElementById('kwhsQuotaRemaining').innerHTML=data +" KWh";
});

socket.on('meterStatus', function(data){
	document.getElementById('meterStatus').innerHTML=data;
});

socket.on('meterChannel', function(data){
	document.getElementById('meterChannel').innerHTML=data;
});

socket.on('linkStrength', function(data){
	document.getElementById('linkStrength').innerHTML=data + "%";
});

socket.on('billEstimate', function(data){
	document.getElementById('billEstimate').innerHTML="$"+ data;
});

socket.on('percentageQuota', function(data){
	document.getElementById('percentageQuota').innerHTML=data +"%";
	document.getElementById('percentageQuota').style.width=data +"%";
	document.getElementById('percentageQuota').style.color="#000";
});

var timestamp = 0;
socket.on('demandTimeStamp', function(data){
timestamp = data;

});

window.setInterval(function(){timer()},1000);
	function timer(){
	count = timestamp ++;
	document.getElementById('demandcounter').innerHTML= count + " seconds ago";}

//Gateway data
socket.on('moteinoTemp', function(data){
	document.getElementById('moteinoTemp').innerHTML=data + " C";
});

socket.on('moteinoHum', function(data){
	document.getElementById('moteinoHum').innerHTML=data + "%";
});

socket.on('remSwitchStatus', function(data){
	if (data == 0){
	document.getElementById('remSwitchStatus').innerHTML="OFF";}
   else if (data == 1){
	document.getElementById('remSwitchStatus').innerHTML="ON";}
});

function switchFunc(data){
	socket.emit('switch', data);
	console.log(data);
}


