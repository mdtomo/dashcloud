// client.js

    // create the module and name it CloudDash
    var cloudDash = angular.module('cloudDash', ['ngRoute']);

    // configure our routes
    cloudDash.config(function($routeProvider) {
        $routeProvider

            // route for the home page
            .when('/', {
                templateUrl : 'pages/home.html',
                controller  : 'mainController'
            })

			// route for the about page
            .when('/history', {
                templateUrl : 'pages/history.html',
                controller  : 'historyController'
            })

            // route for the about page
            .when('/control-panel', {
                templateUrl : 'pages/control-panel.html',
                controller  : 'controlPanelController'
            })

            // route for the contact page
            .when('/logs', {
                templateUrl : 'pages/logs.html',
                controller  : 'logsController'
            });
    });

     // create the controller and inject Angular's $scope
    cloudDash.controller('mainController', function($scope) {
        // create a message to display in our view
        $scope.message = 'Message from Angular controller!';
    });

    cloudDash.controller('historyController', function($scope, $http) {
        $scope.message = 'Historical data controller.';
        $http.get('/api/usage')
            .success(function(data) {
                var kwhsLength = null;
                kwhsLength = data.length - 1;
                var kwhs24HrsAgo = data[0];
                var kwhsLatest = data[kwhsLength];
                var dailyUsage = (kwhsLatest.fromgrid - kwhs24HrsAgo.fromgrid) / 1000; //- data.fromgrid[data.fromgrid.length];
                
                console.log(kwhs24HrsAgo);
                console.log(kwhsLength);
                console.log(kwhsLatest);
                console.log(dailyUsage);

                var cost24Hrs = ((dailyUsage * 11.27) / 100).toFixed(2);
                $scope.usage = dailyUsage;
                $scope.cost = cost24Hrs;
        })
        .error(function(data) {
            console.log('Error: ' + data);
        });

    });

    cloudDash.controller('controlPanelController', function($scope) {
        $scope.message = 'Control panel controller.';
    });

    cloudDash.controller('logsController', function($scope) {
        $scope.message = 'Logger controller.';
    });
