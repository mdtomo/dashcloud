//server.js

var //Eagle API http https
	express 		   = require('express'),
	app				   = express(),
	appSSL			   = express(),
	fs 				   = require('fs'),
	bodyParser		   = require('body-parser'),
	//methodOverride = require('method-override'),
    port 			   = process.env.PORT || 3001,
    securePort 		   = process.env.PORT || 3002,
    morgan			   = require('morgan');

//MongoDB
var	mongoose 		   = require('mongoose');
var	database 		   = require('./config/database');    

/*
    //Passport requirements
var	
	
	cookieParser	   = require('cookie-parser'),
	session			   = require('express-session'),
	passport		   = require('passport'),
	flash			   = require('connect-flash'),
	bcrypt 			   = require('bcrypt-nodejs'),
	LocalStrategy	   = require('passport-local').Strategy;
*/


var	options = {
						 key: fs.readFileSync('./certs/server.key'),
						 cert: fs.readFileSync('./certs/server.crt'),
						 ca: fs.readFileSync('./certs/ca.crt')
	};
	
var	https 			   = require('https').Server(options, appSSL).listen(securePort);
var	http 			   = require('http').Server(app).listen(port);
var socketIO		   = require('./app/socket').listen(https);
//appSSL.set("view engine", "ejs");
//Eagle routes
//var eagleRouter = require('./app/eagleRoutes')(app);
//var sslRouter 	= require('./app/sslRoutes')(appSSL);

	//Express Routers
	//httpsRouter		   = express.Router(),
	//httpRouter		   = express.Router();
	
//Connect to MongoDB database
mongoose.connect(database.url);
	
//appSSL.set("view engine", "ejs");
appSSL.use(express.static(__dirname + '/public'));
appSSL.use(morgan('dev'));
appSSL.use(bodyParser.urlencoded({'extended':'true'}));            // parse application/x-www-form-urlencoded
appSSL.use(bodyParser.json());                                     // parse application/json
appSSL.use(bodyParser.json({ type: 'application/vnd.api+json' })); // parse application/vnd.api+json as json
//app.use(methodOverride());




var eaglePost = require('./app/eagleRoutes');


//Express router at end of app
appSSL.use("/", require('./app/sslRoutes'));
app.use("/", eaglePost.httpRouter);

//exports = module.exports = appSSL;